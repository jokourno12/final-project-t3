<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Detail Pembayaran                      _8d5cc7</name>
   <tag></tag>
   <elementGuidId>3a410213-3ad2-4ba3-b4d3-42afa1dee42a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='ModalLogin']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ModalLogin</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>ccec8789-f8ec-4dc0-b543-3889a82fc0db</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal fade in</value>
      <webElementGuid>cfa73c48-b271-411f-b94a-c50f85adef34</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ModalLogin</value>
      <webElementGuid>06c45daa-77d6-4a91-ad79-0404ab13156d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>-1</value>
      <webElementGuid>0d5e5c15-9ab9-4d1d-aa70-144461772aee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>dialog</value>
      <webElementGuid>03a57d87-5109-4ae3-8d53-524108972853</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            
                

                    

                        
                            ×

                            

                                                            Detail Pembayaran 
                                

                                    
                                        
                                            Total Price
                                        
                                        85.000

                                    
                                    
                                        
                                            Discount Referral / Voucher
                                        
                                        -
                                        
                                    
                                    
                                        
                                            Point Used
                                        
                                        -
                                        
                                    
                                    
                                        
                                            Total Pembayaran
                                        
                                        
                                            85.000
                                        

                                    
                                
                                 Pilih Metode Pembayaran
                                
                                    
                                        
                                            
                                        
                                        
                                            Transfer QR Code / Bank (cek otomatis)
                                        

                                    

                                    
                                        
                                            
                                        
                                        
                                            Sisa Points 
                                                0
                                            
                                        


                                    
                                    
                                        
                                        *Jika poin tidak mencukupi maka sisa pembayaran
                                                dilakukan melalui transfer melalui QR Code
                                        
                                    
                                
                                


                                Confirm
                            


                        


                    
                
                
            
        </value>
      <webElementGuid>a841070b-e0bf-46bb-8271-592877fbc8b2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ModalLogin&quot;)</value>
      <webElementGuid>aa26e635-fc99-4653-a8b7-3ce5d55a859d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='ModalLogin']</value>
      <webElementGuid>fc8ae87e-4157-48ae-8f77-3f1c840d1522</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='EVN30082300027'])[1]/following::div[2]</value>
      <webElementGuid>865d000f-9771-4c5c-ad95-0d6bd5d11517</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div[2]/div[2]</value>
      <webElementGuid>1ec4634b-7fbe-4274-80b3-23e3de3002a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'ModalLogin' and (text() = '
            
                

                    

                        
                            ×

                            

                                                            Detail Pembayaran 
                                

                                    
                                        
                                            Total Price
                                        
                                        85.000

                                    
                                    
                                        
                                            Discount Referral / Voucher
                                        
                                        -
                                        
                                    
                                    
                                        
                                            Point Used
                                        
                                        -
                                        
                                    
                                    
                                        
                                            Total Pembayaran
                                        
                                        
                                            85.000
                                        

                                    
                                
                                 Pilih Metode Pembayaran
                                
                                    
                                        
                                            
                                        
                                        
                                            Transfer QR Code / Bank (cek otomatis)
                                        

                                    

                                    
                                        
                                            
                                        
                                        
                                            Sisa Points 
                                                0
                                            
                                        


                                    
                                    
                                        
                                        *Jika poin tidak mencukupi maka sisa pembayaran
                                                dilakukan melalui transfer melalui QR Code
                                        
                                    
                                
                                


                                Confirm
                            


                        


                    
                
                
            
        ' or . = '
            
                

                    

                        
                            ×

                            

                                                            Detail Pembayaran 
                                

                                    
                                        
                                            Total Price
                                        
                                        85.000

                                    
                                    
                                        
                                            Discount Referral / Voucher
                                        
                                        -
                                        
                                    
                                    
                                        
                                            Point Used
                                        
                                        -
                                        
                                    
                                    
                                        
                                            Total Pembayaran
                                        
                                        
                                            85.000
                                        

                                    
                                
                                 Pilih Metode Pembayaran
                                
                                    
                                        
                                            
                                        
                                        
                                            Transfer QR Code / Bank (cek otomatis)
                                        

                                    

                                    
                                        
                                            
                                        
                                        
                                            Sisa Points 
                                                0
                                            
                                        


                                    
                                    
                                        
                                        *Jika poin tidak mencukupi maka sisa pembayaran
                                                dilakukan melalui transfer melalui QR Code
                                        
                                    
                                
                                


                                Confirm
                            


                        


                    
                
                
            
        ')]</value>
      <webElementGuid>1557cc86-02cf-483e-9baa-70d5b11bcced</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
