<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Pembelian Saya_Day 4 Workshop</name>
   <tag></tag>
   <elementGuidId>271b4a22-62dc-4c38-9af8-914fe2168fca</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#check</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='check']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>67c72e54-4085-4f4e-8797-bab4dcafb0e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>1bd62045-723f-4402-ba24-7aa47c1f51dc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>85000|130|1</value>
      <webElementGuid>53e747a3-86c6-4859-b536-9deea95ee19e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>9837ced0-40d3-4d69-aa9f-38e8c2344d61</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Day 4: Workshop</value>
      <webElementGuid>13c9003b-958a-4733-8bff-1c10a701b80c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data</name>
      <type>Main</type>
      <value>500000</value>
      <webElementGuid>9ae08bf5-b29d-42ae-9f95-d16ccb417e8c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>check</value>
      <webElementGuid>b11d4093-c8ac-4d05-9894-514761987e16</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;check&quot;)</value>
      <webElementGuid>6e540704-4bbf-4065-8802-dc56d63c1dee</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='check']</value>
      <webElementGuid>4b0d4213-e430-4c8e-b353-1132ff9c8ac3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='cartForm']/div/div/div/div/input</value>
      <webElementGuid>8d21e1bb-0501-4c36-a339-c2f6c99eeafc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/input</value>
      <webElementGuid>daec98bd-7ec0-476b-857e-54011d8f9c55</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox' and @name = 'Day 4: Workshop' and @id = 'check']</value>
      <webElementGuid>2a940fee-8cc8-4a86-a866-af222c50988e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
