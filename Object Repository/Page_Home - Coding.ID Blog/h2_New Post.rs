<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h2_New Post</name>
   <tag></tag>
   <elementGuidId>f97bf343-e35a-48fc-9d6d-54c2f81c2617</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h2.elementor-heading-title.elementor-size-default</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='content']/div/div/section[2]/div/div/div/div[2]/div/h2</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h2</value>
      <webElementGuid>139eca1f-224a-4da5-b2b2-62681375b744</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>elementor-heading-title elementor-size-default</value>
      <webElementGuid>ce7d7de2-3ee6-4b8d-ba58-e7328f0112f5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>New Post</value>
      <webElementGuid>becc9500-d6c2-4c63-9530-a4e3e2eea0f9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/div[@class=&quot;ast-container&quot;]/div[@class=&quot;elementor elementor-101&quot;]/section[@class=&quot;elementor-section elementor-top-section elementor-element elementor-element-66bb443a elementor-section-boxed elementor-section-height-default elementor-section-height-default&quot;]/div[@class=&quot;elementor-container elementor-column-gap-default&quot;]/div[@class=&quot;elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-4123552c&quot;]/div[@class=&quot;elementor-widget-wrap elementor-element-populated&quot;]/div[@class=&quot;elementor-element elementor-element-58f23963 elementor-widget elementor-widget-heading&quot;]/div[@class=&quot;elementor-widget-container&quot;]/h2[@class=&quot;elementor-heading-title elementor-size-default&quot;]</value>
      <webElementGuid>ead459bc-db4f-43c2-9af2-66585436c52c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div/div/section[2]/div/div/div/div[2]/div/h2</value>
      <webElementGuid>92e0bedb-414e-49f8-80dd-2839b76da4a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Contact'])[1]/following::h2[1]</value>
      <webElementGuid>02e74295-591c-49fd-85a5-81d4f2323353</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Blog'])[1]/following::h2[1]</value>
      <webElementGuid>7ac47e84-aac5-4030-927b-3ba72328d75b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Info Technology'])[1]/preceding::h2[1]</value>
      <webElementGuid>538c8c1f-1174-4f14-920d-87d7e74207b9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='New Post']/parent::*</value>
      <webElementGuid>a5082d37-bbb5-487c-89ba-2fe36e84eb60</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h2</value>
      <webElementGuid>58b3d3b6-1361-44b2-801d-89178ee70a41</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h2[(text() = 'New Post' or . = 'New Post')]</value>
      <webElementGuid>a5b3f4b3-edcc-44a4-88d0-bc412ff29d0b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
