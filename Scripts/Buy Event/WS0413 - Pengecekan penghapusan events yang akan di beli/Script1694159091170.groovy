import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

CustomKeywords.'auth.Authenticate.performLogin'()

WebUI.doubleClick(findTestObject('Object Repository/Event/Page_Online event bersertifikat dari prakti_f42b96/li_My                                      _41f013'))

WebUI.click(findTestObject('Object Repository/Event/Page_Online event bersertifikat dari prakti_f42b96/a_Checkout'))

WebUI.navigateToUrl('https://demo-app.online/view_cart')

WebUI.verifyElementVisible(findTestObject('Object Repository/Event/Page_Coding.ID - Cart/h5_Total'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementText(findTestObject('Object Repository/Event/Page_Coding.ID - Cart/h5_Total'), 'Total')

WebUI.click(findTestObject('Object Repository/Event/Page_Coding.ID - Cart/p_Remove'))

WebUI.navigateToUrl('https://demo-app.online/view_cart')

WebUI.verifyElementVisible(findTestObject('Object Repository/Event/Page_Coding.ID - Cart/h4_Ooopps seems like your cart is empty'))

WebUI.verifyElementText(findTestObject('Object Repository/Event/Page_Coding.ID - Cart/h4_Ooopps seems like your cart is empty'), 
    'Ooopps seems like your cart is empty')

WebUI.closeBrowser()

